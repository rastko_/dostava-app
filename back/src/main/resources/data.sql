INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
              
INSERT INTO dostavljac (id, ime_prezime, jmbg, lk)
	VALUES (1, "Dostavljac 1", "1111111111111", "111111");
INSERT INTO dostavljac (id, ime_prezime, jmbg, lk)
	VALUES (2, "Dostavljac 2", "2222222222222", "222222");
INSERT INTO dostavljac (id, ime_prezime, jmbg, lk)
	VALUES (3, "Dostavljac 3", "3333333333333", "333333");
	
INSERT INTO narudzba (id, br_narudzbe, cena, datum, mesto_isporuke, opis, dostavljac_id)
	VALUES (1, 11111, 111.1, "2021-01-10", "Mesto 1", "Opis 1", 1);
INSERT INTO narudzba (id, br_narudzbe, cena, datum, mesto_isporuke, opis, dostavljac_id)
	VALUES (2, 22222, 122.1, "2021-01-02", "Adresa 1", "Opis 2", 2);
INSERT INTO narudzba (id, br_narudzbe, cena, datum, mesto_isporuke, opis, dostavljac_id)
	VALUES (3, 33333, 31.1, "2021-01-01", "Mesto 2", "Opis 3", 3);
INSERT INTO narudzba (id, br_narudzbe, cena, datum, mesto_isporuke, opis, dostavljac_id)
	VALUES (4, 44444, 441.1, "2021-01-03", "Adresa 2", "Opis 4", 1);
INSERT INTO narudzba (id, br_narudzbe, cena, datum, mesto_isporuke, opis, dostavljac_id)
	VALUES (5, 55555, 5555.1, "2021-01-05", "Mesto 3", "Opis 5", 1);
	
INSERT INTO racun (id, br_racuna, datum, ukupna_cena, narudzba_id)
	VALUES (1, 11111, "2021-02-02", 111.1, 1);
INSERT INTO racun (id, br_racuna, datum, ukupna_cena, narudzba_id)
	VALUES (2, 22222, "2021-02-03", 122.1, 2);
INSERT INTO racun (id, br_racuna, datum, ukupna_cena, narudzba_id)
	VALUES (3, 33333, "2021-02-04", 31.1, 3);
package com.modul3.test.jwd48rnTMP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.Racun;
import com.modul3.test.jwd48rnTMP.repository.RacunRepository;
import com.modul3.test.jwd48rnTMP.service.RacunService;

@Service
public class JpaRacunService implements RacunService {

	@Autowired
	private RacunRepository racunRepository;
	
	@Override
	public Racun findOne(Long id) {
		// TODO Auto-generated method stub
		return racunRepository.findOneById(id);
	}

	@Override
	public List<Racun> findAll() {
		// TODO Auto-generated method stub
		return racunRepository.findAll();
	}

	@Override
	public Racun save(Racun racun) {
		// TODO Auto-generated method stub
		return racunRepository.save(racun);
	}

}

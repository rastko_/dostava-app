package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Racun;
import com.modul3.test.jwd48rnTMP.web.dto.RacunDTO;

@Component
public class RacunToRacunDto implements Converter<Racun, RacunDTO> {

	@Override
	public RacunDTO convert(Racun racun) {
		RacunDTO dto = new RacunDTO();
		dto.setId(racun.getId());
		dto.setBrRacuna(racun.getBrRacuna());
		dto.setDatum(racun.getDatum().toString());
		if (racun.getNarudzba() != null) {
			dto.setNarudzbaId(racun.getNarudzba().getId());
		}
		dto.setUkupnaCena(racun.getUkupnaCena());
		return dto;
	}
	public List<RacunDTO> convert (List<Racun> racuni){
		List<RacunDTO> dtos = new ArrayList<>();
		for (Racun racun : racuni) {
			dtos.add(convert(racun));
		}
		return dtos;
	}
}

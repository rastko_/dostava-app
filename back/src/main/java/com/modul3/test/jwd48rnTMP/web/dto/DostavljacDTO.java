package com.modul3.test.jwd48rnTMP.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class DostavljacDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	@NotBlank (message = "JMBG - nije zadato.")
	@NotNull
	@Size(min = 13, max = 13)
	private String jmbg;
	
	@NotBlank (message = "Licna karta - nije zadato.")
	@NotNull
	private String lk;
	
	@NotBlank (message = "Ime i prezime - nije zadato.")
	@NotNull
	private String imePrezime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getLk() {
		return lk;
	}

	public void setLk(String lk) {
		this.lk = lk;
	}

	public String getImePrezime() {
		return imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}
	
	
}

package com.modul3.test.jwd48rnTMP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.Dostavljac;
import com.modul3.test.jwd48rnTMP.repository.DostavljacRepository;
import com.modul3.test.jwd48rnTMP.service.DostavljacService;

@Service
public class JpaDostavljacService implements DostavljacService {

	@Autowired
	private DostavljacRepository dostavljacRepository;
	
	@Override
	public Dostavljac findOne(Long id) {
		// TODO Auto-generated method stub
		return dostavljacRepository.findOneById(id);
	}

	@Override
	public List<Dostavljac> findAll() {
		// TODO Auto-generated method stub
		return dostavljacRepository.findAll();
	}

	@Override
	public Dostavljac save(Dostavljac dostavljac) {
		// TODO Auto-generated method stub
		return dostavljacRepository.save(dostavljac);
	}

}

package com.modul3.test.jwd48rnTMP.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Racun;
import com.modul3.test.jwd48rnTMP.service.NarudzbaService;
import com.modul3.test.jwd48rnTMP.service.RacunService;
import com.modul3.test.jwd48rnTMP.web.dto.RacunDTO;

@Component
public class RacunDtoToRacun implements Converter<RacunDTO, Racun> {

	@Autowired
	private RacunService racunService;
	
	@Autowired
	private NarudzbaService narudzbaService;
	
	@Override
	public Racun convert(RacunDTO dto) {
		Racun racun;
		if (dto.getId() != null) {
			racun = racunService.findOne(dto.getId());
		}else {
			racun = new Racun();
		}
		if (racun != null) {
			if (dto.getBrRacuna() != null) {
				racun.setBrRacuna(dto.getBrRacuna());
			}
			if (dto.getDatum() != null) {
				racun.setDatum(getLocalDate(dto.getDatum()));
			}
			if (dto.getNarudzbaId() != null) {
				racun.setNarudzba(narudzbaService.findOne(dto.getNarudzbaId()));
			}
			if (dto.getUkupnaCena() != null) {
				racun.setUkupnaCena(dto.getUkupnaCena());
			}
		}
		return racun;
	}

	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}
}

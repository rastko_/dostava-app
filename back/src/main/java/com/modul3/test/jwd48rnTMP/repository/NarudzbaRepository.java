package com.modul3.test.jwd48rnTMP.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.Narudzba;

@Repository
public interface NarudzbaRepository extends JpaRepository<Narudzba, Long> {

	Narudzba findOneById(Long id);
	
	List<Narudzba> findByIdIn(List<Long> ids);
	
	@Query("SELECT n FROM Narudzba n WHERE " +
			"(:pretragaDatum = NULL OR n.datum >= :pretragaDatum) AND " +
            "(:mestoIsporuke = NULL OR n.mestoIsporuke LIKE %:mestoIsporuke%) AND " +
            "(:dostavljacId = NULL OR n.dostavljac.id = :dostavljacId) " +
			"ORDER BY n.datum")
	Page<Narudzba> search(@Param("dostavljacId")Long dostavljacId, @Param("mestoIsporuke")String mestoIsporuke, @Param("pretragaDatum")LocalDate pretragaDatum, Pageable pageable);
	
	@Query(value = "SELECT COALESCE(SUM(n.cena), 0) FROM Narudzba n WHERE " +
			"dostavljac_id = :dostavljacId", nativeQuery = true)
	Double zbirCenaNarudzbiPoDostavljacu(@Param("dostavljacId")Long dostavljacId);
}

package com.modul3.test.jwd48rnTMP.service;

import java.util.List;

import com.modul3.test.jwd48rnTMP.model.Racun;

public interface RacunService {

	Racun findOne(Long id);
	
	List<Racun> findAll();
	
	Racun save(Racun racun);	
}

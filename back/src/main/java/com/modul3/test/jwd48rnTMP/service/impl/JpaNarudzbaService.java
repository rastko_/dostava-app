package com.modul3.test.jwd48rnTMP.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.modul3.test.jwd48rnTMP.model.Narudzba;
import com.modul3.test.jwd48rnTMP.repository.NarudzbaRepository;
import com.modul3.test.jwd48rnTMP.service.NarudzbaService;

@Service
public class JpaNarudzbaService implements NarudzbaService {

	@Autowired
	private NarudzbaRepository narudzbaRepository;
	
	@Override
	public Narudzba findOne(Long id) {
		// TODO Auto-generated method stub
		return narudzbaRepository.findOneById(id);
	}

	@Override
	public List<Narudzba> findAll() {
		// TODO Auto-generated method stub
		return narudzbaRepository.findAll();
	}

	@Override
	public List<Narudzba> find(List<Long> ids) {
		// TODO Auto-generated method stub
		return narudzbaRepository.findByIdIn(ids);
	}

	@Override
	public Page<Narudzba> findAll(int pageNo) {
		// TODO Auto-generated method stub
		return narudzbaRepository.findAll(PageRequest.of(pageNo, 4));
	}

	@Override
	public Narudzba save(Narudzba narudzba) {
		// TODO Auto-generated method stub
		return narudzbaRepository.save(narudzba);
	}

	@Override
	public Narudzba update(Narudzba narudzba) {
		// TODO Auto-generated method stub
		return narudzbaRepository.save(narudzba);
	}

	@Override
	public Narudzba delete(Long id) {
		Narudzba narudzbaDelete = findOne(id);
		if (narudzbaDelete != null) {
			narudzbaRepository.delete(narudzbaDelete);
			return narudzbaDelete;
		}
		return null;
	}

	@Override
	public Page<Narudzba> find(Long dostavljacId, String mestoIsporuke, LocalDate pretragaDatum, int pageNo) {
		// TODO Auto-generated method stub
		return narudzbaRepository.search(dostavljacId, mestoIsporuke, pretragaDatum, PageRequest.of(pageNo, 4));
	}

	@Override
	public Double zbirCenaNarudzbiPoDostavljacu(Long dostavljacId) {
		// TODO Auto-generated method stub
		return narudzbaRepository.zbirCenaNarudzbiPoDostavljacu(dostavljacId);
	}

}

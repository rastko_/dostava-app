package com.modul3.test.jwd48rnTMP.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Dostavljac;
import com.modul3.test.jwd48rnTMP.service.DostavljacService;
import com.modul3.test.jwd48rnTMP.support.DostavljacToDostavljacDto;
import com.modul3.test.jwd48rnTMP.web.dto.DostavljacDTO;

@RestController
@RequestMapping(value = "/api/dostavljaci", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class DostavljacController {

	@Autowired
	private DostavljacService dostavljacService;
	
	@Autowired
	private DostavljacToDostavljacDto toDostavljacDto;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<DostavljacDTO> getOne(@PathVariable Long id){
		Dostavljac dostavljac = dostavljacService.findOne(id);
		if (dostavljac != null) {
			return new ResponseEntity<>(toDostavljacDto.convert(dostavljac), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<DostavljacDTO>> getAll(){
		List<Dostavljac> dostavljaci = dostavljacService.findAll();
		return new ResponseEntity<>(toDostavljacDto.convert(dostavljaci), HttpStatus.OK);
	}
}

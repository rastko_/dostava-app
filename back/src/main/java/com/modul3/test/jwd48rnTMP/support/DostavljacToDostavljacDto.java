package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Dostavljac;
import com.modul3.test.jwd48rnTMP.web.dto.DostavljacDTO;

@Component
public class DostavljacToDostavljacDto implements Converter<Dostavljac, DostavljacDTO> {

	@Override
	public DostavljacDTO convert(Dostavljac dostavljac) {
		DostavljacDTO dto = new DostavljacDTO();
		dto.setId(dostavljac.getId());
		dto.setImePrezime(dostavljac.getImePrezime());
		dto.setJmbg(dostavljac.getJmbg());
		dto.setLk(dostavljac.getLk());
		return dto;
	}
	public List<DostavljacDTO> convert (List<Dostavljac> dostavljaci){
		List<DostavljacDTO> dtos = new ArrayList<>();
		for (Dostavljac dostavljac : dostavljaci) {
			dtos.add(convert(dostavljac));
		}
		return dtos;
	}
}

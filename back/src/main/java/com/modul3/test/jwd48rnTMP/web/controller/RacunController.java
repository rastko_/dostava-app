package com.modul3.test.jwd48rnTMP.web.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Racun;
import com.modul3.test.jwd48rnTMP.service.NarudzbaService;
import com.modul3.test.jwd48rnTMP.service.RacunService;
import com.modul3.test.jwd48rnTMP.support.RacunDtoToRacun;
import com.modul3.test.jwd48rnTMP.support.RacunToRacunDto;
import com.modul3.test.jwd48rnTMP.web.dto.RacunDTO;

@RestController
@RequestMapping(value = "/api/racuni", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class RacunController {

	@Autowired
	private RacunService racunService;
	
	@Autowired
	private NarudzbaService narudzbaService;
	
	@Autowired
	private RacunToRacunDto toRacunDto;
	
	@Autowired
	private RacunDtoToRacun toRacun;
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<RacunDTO> getOne(@PathVariable Long id){
		Racun racun = racunService.findOne(id);
		if (racun != null) {
			return new ResponseEntity<>(toRacunDto.convert(racun), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<RacunDTO>> getAll(){
		List<Racun> racuni = racunService.findAll();
		return new ResponseEntity<>(toRacunDto.convert(racuni), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RacunDTO> create(@Valid @RequestBody RacunDTO racunDTO){
		System.out.println();
		System.out.println(racunDTO);
		System.out.println();
		
		Racun racun = toRacun.convert(racunDTO);
		
		LocalDate danasnjiDatum = LocalDate.now();
		racun.setDatum(danasnjiDatum);
		racun.setBrRacuna(racun.getNarudzba().getBrNarudzbe());
		racun.setUkupnaCena(racun.getNarudzba().getCena());
		
		Racun sacuvanRacun = racunService.save(racun);
		return new ResponseEntity<>(toRacunDto.convert(sacuvanRacun), HttpStatus.CREATED);
	}
}

package com.modul3.test.jwd48rnTMP.web.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.modul3.test.jwd48rnTMP.model.Narudzba;
import com.modul3.test.jwd48rnTMP.service.DostavljacService;
import com.modul3.test.jwd48rnTMP.service.NarudzbaService;
import com.modul3.test.jwd48rnTMP.service.RacunService;
import com.modul3.test.jwd48rnTMP.support.NarudzbaDtoToNarudzba;
import com.modul3.test.jwd48rnTMP.support.NarudzbaToNarudzbaDto;
import com.modul3.test.jwd48rnTMP.web.dto.NarudzbaDTO;

@RestController
@RequestMapping(value = "/api/narudzbe", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class NarudzbaController {

	@Autowired
	private NarudzbaService narudzbaService;
	
	@Autowired
	private DostavljacService dostavljacService;
	
	@Autowired
	private RacunService racunService;
	
	@Autowired
	private NarudzbaToNarudzbaDto toNarudzbaDto;
	
	@Autowired
	private NarudzbaDtoToNarudzba toNarudzba;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<NarudzbaDTO> getOne(@PathVariable Long id){
		Narudzba narudzba = narudzbaService.findOne(id);
		if (narudzba != null) {
			return new ResponseEntity<>(toNarudzbaDto.convert(narudzba), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<NarudzbaDTO>> getAll(
			@RequestParam(required = false) Long dostavljacId,
			@RequestParam(required = false) String mestoIsporuke,
			@RequestParam(required = false) String pretragaDatumString,
			@RequestParam(value="pageNo", defaultValue = "0") int pageNo){
		
		Page<Narudzba> pageNarudzba;
		
		if (mestoIsporuke != null) {
			if (mestoIsporuke.trim() == "") {
				mestoIsporuke = null;
			}
		}
		if (dostavljacId != null) {
			if (dostavljacId == -1) {
				dostavljacId = null;
			}
		}
		LocalDate pretragaDatum;
		if (pretragaDatumString != null) {
			pretragaDatum = getLocalDate(pretragaDatumString);
		} else {
			pretragaDatum = LocalDate.of(1900, 01, 01);
		}
	
		
		pageNarudzba = narudzbaService.find(dostavljacId, mestoIsporuke, pretragaDatum, pageNo);
		
//		pageNarudzba = narudzbaService.findAll(pageNo);
		HttpHeaders headers = new HttpHeaders();
		headers.add("total-pages", Integer.toString(pageNarudzba.getTotalPages()));
		
		if (dostavljacId != null) {
			Double zbirCenaNarudzbiPoDostavljacu = narudzbaService.zbirCenaNarudzbiPoDostavljacu(dostavljacId);
			headers.add("zbir-cena-dostavljac", zbirCenaNarudzbiPoDostavljacu.toString());
		}
		
		return new ResponseEntity<>(toNarudzbaDto.convert(pageNarudzba.getContent()), headers, HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<NarudzbaDTO> create(@Valid @RequestBody NarudzbaDTO narudzbaDTO){
		System.out.println();
		System.out.println(narudzbaDTO);
		System.out.println();
		Narudzba narudzba = toNarudzba.convert(narudzbaDTO);
		if (narudzba.getDostavljac() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Narudzba sacuvanaNarudzba = narudzbaService.save(narudzba);
		return new ResponseEntity<>(toNarudzbaDto.convert(sacuvanaNarudzba), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<NarudzbaDTO> update(@PathVariable Long id, @Valid @RequestBody NarudzbaDTO narudzbaDTO){
		if (narudzbaDTO.getId() != id) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Narudzba narudzba = toNarudzba.convert(narudzbaDTO);
		if (narudzba.getDostavljac() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Narudzba updateNarudzba = narudzbaService.update(narudzba);
		return new ResponseEntity<>(toNarudzbaDto.convert(updateNarudzba), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		Narudzba narudzba = narudzbaService.delete(id);
		System.out.println();
		System.out.println(narudzba);
		System.out.println();
		if (narudzba == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
	}
	
	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}
}

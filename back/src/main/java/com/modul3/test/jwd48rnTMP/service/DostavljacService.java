package com.modul3.test.jwd48rnTMP.service;

import java.util.List;

import com.modul3.test.jwd48rnTMP.model.Dostavljac;

public interface DostavljacService {

	Dostavljac findOne(Long id);
	
	List<Dostavljac> findAll();
	
	Dostavljac save(Dostavljac dostavljac);
}

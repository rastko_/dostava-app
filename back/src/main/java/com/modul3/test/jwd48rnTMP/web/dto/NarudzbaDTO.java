package com.modul3.test.jwd48rnTMP.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class NarudzbaDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	@Positive (message = "Broj narudzbe mora da bude pozitivan broj.")
	private Integer brNarudzbe;
	
	@NotBlank (message = "Datum - nije zadato.")
	@NotNull
	@Pattern(regexp = "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$", message = "Datum nije validan.")
	private String datum;
	
	@NotBlank (message = "JMBG - nije zadato.")
	@NotNull
	@Size(max = 50)
	private String mestoIsporuke;
	
	@Positive (message = "Cena mora da bude pozitivan broj.")
	private Double cena;
	
	private String opis;
	
	private Long dostavljacId;
	
	private String dostavljacImePrezime;
	
	private Long racunId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrNarudzbe() {
		return brNarudzbe;
	}

	public void setBrNarudzbe(Integer brNarudzbe) {
		this.brNarudzbe = brNarudzbe;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getMestoIsporuke() {
		return mestoIsporuke;
	}

	public void setMestoIsporuke(String mestoIsporuke) {
		this.mestoIsporuke = mestoIsporuke;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Long getDostavljacId() {
		return dostavljacId;
	}

	public void setDostavljacId(Long dostavljacId) {
		this.dostavljacId = dostavljacId;
	}

	public String getDostavljacImePrezime() {
		return dostavljacImePrezime;
	}

	public void setDostavljacImePrezime(String dostavljacImePrezime) {
		this.dostavljacImePrezime = dostavljacImePrezime;
	}

	public Long getRacunId() {
		return racunId;
	}

	public void setRacunId(Long racunId) {
		this.racunId = racunId;
	}
	
	
}

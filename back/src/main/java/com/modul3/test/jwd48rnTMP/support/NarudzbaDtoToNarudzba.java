package com.modul3.test.jwd48rnTMP.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Narudzba;
import com.modul3.test.jwd48rnTMP.service.DostavljacService;
import com.modul3.test.jwd48rnTMP.service.NarudzbaService;
import com.modul3.test.jwd48rnTMP.service.RacunService;
import com.modul3.test.jwd48rnTMP.web.dto.NarudzbaDTO;

@Component
public class NarudzbaDtoToNarudzba implements Converter<NarudzbaDTO, Narudzba> {

	@Autowired
	private NarudzbaService narudzbaService;
	
	@Autowired
	private DostavljacService dostavljacService;
	
	@Autowired
	private RacunService racunService;
	
	@Override
	public Narudzba convert(NarudzbaDTO dto) {
		Narudzba narudzba;
		if (dto.getId() != null) {
			narudzba = narudzbaService.findOne(dto.getId());
		} else {
			narudzba = new Narudzba();
		}
		if (narudzba != null) {
			narudzba.setBrNarudzbe(dto.getBrNarudzbe());
			narudzba.setCena(dto.getCena());
			narudzba.setDatum(getLocalDate(dto.getDatum()));
			if (dto.getDostavljacId() != null) {
				narudzba.setDostavljac(dostavljacService.findOne(dto.getDostavljacId()));
			}
			narudzba.setMestoIsporuke(dto.getMestoIsporuke());
			narudzba.setOpis(dto.getOpis());
			if (dto.getRacunId() != null) {
				narudzba.setRacun(racunService.findOne(dto.getRacunId()));
			}
		}
		return narudzba;
	}

	private LocalDate getLocalDate(String date) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);
	}
}

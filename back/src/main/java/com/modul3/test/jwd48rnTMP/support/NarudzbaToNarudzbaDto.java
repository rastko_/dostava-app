package com.modul3.test.jwd48rnTMP.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.modul3.test.jwd48rnTMP.model.Narudzba;
import com.modul3.test.jwd48rnTMP.web.dto.NarudzbaDTO;

@Component
public class NarudzbaToNarudzbaDto implements Converter<Narudzba, NarudzbaDTO> {

	@Override
	public NarudzbaDTO convert(Narudzba narudzba) {
		NarudzbaDTO dto = new NarudzbaDTO();
		dto.setId(narudzba.getId());
		dto.setBrNarudzbe(narudzba.getBrNarudzbe());
		dto.setCena(narudzba.getCena());
		dto.setDatum(narudzba.getDatum().toString());
		if (narudzba.getDostavljac() != null) {
			dto.setDostavljacId(narudzba.getDostavljac().getId());
			dto.setDostavljacImePrezime(narudzba.getDostavljac().getImePrezime());
		}
		
		dto.setMestoIsporuke(narudzba.getMestoIsporuke());
		dto.setOpis(narudzba.getOpis());
		if (narudzba.getRacun() != null) {
			dto.setRacunId(narudzba.getRacun().getId());

		}
		
		return dto;
	}
	public List<NarudzbaDTO> convert(List<Narudzba> narudzbe){
		List<NarudzbaDTO> dtos = new ArrayList<>();
		for (Narudzba narudzba : narudzbe) {
			dtos.add(convert(narudzba));
		}
		return dtos;
	}
}

package com.modul3.test.jwd48rnTMP.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;

import com.modul3.test.jwd48rnTMP.model.Narudzba;

public interface NarudzbaService {

	Narudzba findOne(Long id);
	
	List<Narudzba> findAll();
	
	List<Narudzba> find(List<Long> ids);
	
	Page<Narudzba> findAll(int pageNo);
	
	Narudzba save(Narudzba narudzba);
	
	Narudzba update(Narudzba narudzba);
	
	Narudzba delete(Long id);

	Page<Narudzba> find(Long dostavljacId, String mestoIsporuke, LocalDate pretragaDatum, int pageNo);
	
	Double zbirCenaNarudzbiPoDostavljacu (Long dostavljacId);
}

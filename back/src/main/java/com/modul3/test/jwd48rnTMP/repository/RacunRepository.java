package com.modul3.test.jwd48rnTMP.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.modul3.test.jwd48rnTMP.model.Racun;

@Repository
public interface RacunRepository extends JpaRepository<Racun, Long> {

	Racun findOneById(Long id);
}

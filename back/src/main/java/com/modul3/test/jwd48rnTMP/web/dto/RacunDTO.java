package com.modul3.test.jwd48rnTMP.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

public class RacunDTO {

	@Positive (message = "Id mora da bude pozitivan broj.")
	private Long id;
	
	@Positive (message = "Broj racuna mora da bude pozitivan broj.")
	private Integer brRacuna;
	
	private String datum;
	
	@Positive (message = "Ukupna cena mora da bude pozitivan broj.")
	private Double ukupnaCena;
	
	private Long narudzbaId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrRacuna() {
		return brRacuna;
	}

	public void setBrRacuna(Integer brRacuna) {
		this.brRacuna = brRacuna;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public Double getUkupnaCena() {
		return ukupnaCena;
	}

	public void setUkupnaCena(Double ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}

	public Long getNarudzbaId() {
		return narudzbaId;
	}

	public void setNarudzbaId(Long narudzbaId) {
		this.narudzbaId = narudzbaId;
	}
	
	
}

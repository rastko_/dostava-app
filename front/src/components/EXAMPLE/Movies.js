import React from 'react';
import {Table, Button} from 'react-bootstrap'
import CinemaAxios from './../../apis/CinemaAxios';

class Movies extends React.Component {

    constructor(props) {
        super(props);
        this.functionForMovieEdit = props.selectMovie
        this.state = { movies: []}
    }

    componentDidMount() {
        this.getMovies();
    }

    getMovies() {
        CinemaAxios.get('/filmovi')
            .then(res => {
                 // handle success
                 console.log(res);
                 this.setState({movies: res.data});
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    getGenresStringFromList(map) {
        return Object.values(map).join(",");
    }

    goToEdit(movieId) {
        this.functionForMovieEdit(this.state.movies.find(movie=>movie.id==movieId))
        this.props.history.push('/movies/add'); 
    }

    deleteFromState(movieId) {
        var movies = this.state.movies;
        movies.forEach((element, index) => {
            if (element.id === movieId) {
                movies.splice(index, 1);
                this.setState({movies: movies});
            }
        });
    }

    delete(movieId) {
        CinemaAxios.delete('/filmovi/' + movieId)
        .then(res => {
            // handle success
            console.log(res);
            alert('Movie was deleted successfully!');
            this.deleteFromState(movieId); // ili refresh page-a window.location.reload();
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    goToAdd() {
        this.props.history.push('/movies/add');  
    }


    renderMovies() {
        return this.state.movies.map((movie, index) => {
            return (
               <tr key={movie.id}>
                  <td>{movie.naziv}</td>
                  <td>{movie.trajanje}</td>
                  <td>{this.getGenresStringFromList(movie.zanrovi)}</td>
                  <td><Button variant="warning" onClick={() => this.goToEdit(movie.id)}>Edit</Button></td>
                  <td><Button variant="danger" onClick={() => this.delete(movie.id)}>Delete</Button></td>
               </tr>
            )
         })
    }

    render() {
        return (
            <div>
                <h1>Movies</h1>
                
                <div>
                    <Button onClick={() => this.goToAdd() }>Add</Button>
                    <br/>
                    
                    <Table style={{marginTop:5}}>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Duration (min)</th>
                                <th>Genres</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderMovies()}
                        </tbody>                  
                    </Table>
                </div>
            </div>
        );
    }
}

export default Movies;
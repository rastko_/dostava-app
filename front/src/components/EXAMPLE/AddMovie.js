import React from "react";
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import CinemaAxios from "./../../apis/CinemaAxios";
import SelectGenres from "./SelectGenres";

class AddMovie extends React.Component {
  constructor(props) {
    super(props);
    const movie = this.props.selectedMovie==""?{
      id : -1,
      naziv: "",
      trajanje: 0,
      zanrovi: {}
    }:this.props.selectedMovie
    this.props.deleteMovie("");
    this.state = {
      movie: movie,
      showSelectGenres: false
    };
    this.create = this.create.bind(this);
    this.handleGenreSelection = this.handleGenreSelection.bind(this);
  }

  handleGenreSelection(selectedGenres) {
    this.setState({ selectedGenres: selectedGenres });
  }

  create() {
    var params = {
      naziv: this.state.movie.naziv,
      trajanje: this.state.movie.trajanje,
      zanrovi: this.state.movie.zanrovi
    };

    if(this.state.movie.id==-1){
      CinemaAxios.post("/filmovi", params)
      .then((res) => {
        // handle success
        console.log(res);

        alert("Movie was added successfully!");
        this.props.history.push("/movies");
      })
      .catch((error) => {
        // handle error
        console.log(error);
        alert("Error occured please try again!");
      });
    }else{
      params['id'] = this.state.movie.id
      CinemaAxios.put("/filmovi/"+this.state.movie.id, params)
      .then((res) => {
        // handle success
        console.log(res);

        alert("Movie was added successfully!");
        this.props.history.push("/movies");
      })
      .catch((error) => {
        // handle error
        console.log(error);
        alert("Error occured please try again!");
      });
    }
    
  }

  onNameChange = (event) => {
    let movie = JSON.parse(JSON.stringify(this.state.movie))
    movie['naziv'] = event.target.value

    this.setState({movie:movie});
  };

  onDurationChange = (event) => {
    let movie = JSON.parse(JSON.stringify(this.state.movie))
    movie['trajanje'] = event.target.value

    this.setState({movie:movie});
  };

  summarizedGenres(){
    return Object.values(this.state.movie.zanrovi).join(', ')
  }

  render() {
    return (
      <>
        <Row>
          <Col></Col>
          <Col xs="12" sm="10" md="8">
            <h1>Add new movie</h1>
            <Form>
              <Form.Label htmlFor="name">Name</Form.Label>
              <Form.Control
              value={this.state.movie.naziv}
                id="name"
                type="text"
                onChange={(e) => this.onNameChange(e)}
              />
              <Form.Label htmlFor="duration">Duration</Form.Label>
              <Form.Control
                            value={this.state.movie.trajanje}
                id="duration"
                type="number"
                onChange={(e) => this.onDurationChange(e)}
              />

              <Form.Label>Genres</Form.Label>
              <InputGroup>
                <Form.Control value={this.summarizedGenres()} disabled />
                <InputGroup.Append>
                  <Button
                    variant="info"
                    onClick={() => this.setState({ showSelectGenres: true })}
                  >
                    &gt;
                  </Button>
                </InputGroup.Append>
              </InputGroup>

              <Button style={{ marginTop: "25px" }} onClick={this.create}>
                {this.state.movie.id==-1?"Add":"Edit"}
              </Button>
            </Form>
          </Col>
          <Col></Col>
        </Row>

        <SelectGenres
          show={this.state.showSelectGenres}
          handleClose={() => this.setState({ showSelectGenres: false })}
          selectedGenres={this.state.movie.zanrovi}
          finishSelection={(newlySelectedGenres) =>
            {
            let zanrovi= {}
            newlySelectedGenres.map(genre => zanrovi[genre.id]=genre.naziv)
            let movie = JSON.parse(JSON.stringify(this.state.movie))
            movie.zanrovi = zanrovi
            this.setState({
              movie: movie,
              showSelectGenres: false,
            })}
          }
        />
      </>
    );
  }
}

export default AddMovie;

import React from "react";
import CinemaAxios from "../../apis/CinemaAxios";
import {Button, ButtonGroup, Table, Form, Col, Row} from 'react-bootstrap';

class Projections extends React.Component {
  constructor(props) {
    super(props);

    const search = {
      minPrice: "",
      maxPrice: "",
      movieId: -1,
      from: "",
      until: ""
    }
    this.state = { 
      projections: [],
      movies: [],
      pageNo: 0,
      totalPages: 1,
      search: search
    }
  }

  componentDidMount() {
    this.getProjections(0);
    this.getMovies();
  }

  async getMovies(){
    try{
      const result = await CinemaAxios.get("/filmovi");
      this.setState(
        {
          movies: result.data
        }
      )
    }catch(error){
      console.log(error);
    }
  }
  //TODO prokomentarisati sto sam promenio da se radi sa pageNo, a ne changeDir!!
  async getProjections(newPageNo) {
   
    const config = {
      params: {
        pageNo: newPageNo
      }
    }

    if(this.state.search.minPrice!=""){
      config.params['cenaKarteOd'] = this.state.search.minPrice
    }
    if(this.state.search.maxPrice!=""){
      config.params['cenaKarteDo'] = this.state.search.maxPrice
    }
    if(this.state.search.movieId!=-1){
      config.params['filmId'] = this.state.search.movieId
    }
    if(this.state.search.from!=""){
      config.params['datumIVremeOdParametar'] = this.state.search.from
    }
    if(this.state.search.until!=""){
      config.params['datumIVremeDoParametar'] = this.state.search.until
    }
    try {
      let result = await CinemaAxios.get("/projekcije", config);
      this.setState({
        projections: result.data,
        pageNo: newPageNo,
        totalPages: result.headers['total-pages']
        });
    }catch (error) {
      console.log(error);
    }
  }

  goToAdd() {
    this.props.history.push("/projections/add");
  }

  searchValueInputChange(event){
    const name = event.target.name;
    const value = event.target.value;

    let search = {...this.state.search};
    search[name] = value
    this.setState({search:search})
  }

  render() {
    return (
      <div>
        <h1>Projections</h1>

        <Form style={{marginTop:35}}>

        <Row>
          <Col md={6}>
            <Form.Group>
            <Form.Label>From</Form.Label>
            <Form.Control
              value={this.state.search.from}
              name="from"
              as="input"
              type="text"
              onChange={(e) => this.searchValueInputChange(e)}
            ></Form.Control>
          </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group>
            <Form.Label>Until</Form.Label>
            <Form.Control
              value={this.state.search.until}
              name="until"
              as="input"
              type="text"
              onChange={(e) => this.searchValueInputChange(e)}
            ></Form.Control>
          </Form.Group>
          </Col>
          </Row>

        <Row>
          <Col md={6}>
            <Form.Group>
            <Form.Label>Min Price</Form.Label>
            <Form.Control
              value={this.state.search.minPrice}
              name="minPrice"
              as="input"
              type="number"
              onChange={(e) => this.searchValueInputChange(e)}
            ></Form.Control>
          </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group>
            <Form.Label>Max Price</Form.Label>
            <Form.Control
              value={this.state.search.maxPrice}
              name="maxPrice"
              as="input"
              type="number"
              onChange={(e) => this.searchValueInputChange(e)}
            ></Form.Control>
          </Form.Group>
          </Col>
          </Row>
          
          <Form.Group>
            <Form.Label>Movie</Form.Label>
            <Form.Control
              onChange={(event) => this.searchValueInputChange(event)}
              name="movieId"
              value={this.state.search.movieId}
              as="select">
              <option value={-1}></option>
              {this.state.movies.map((movie) => {
                return (
                  <option value={movie.id} key={movie.id}>
                    {movie.naziv}
                  </option>
                );
              })}
            </Form.Control>
          </Form.Group>
          <Button onClick={() => this.getProjections(0)}>Search</Button>
        </Form>

        <br/><br/>
        <div>
          <Button onClick={() => this.goToAdd()}>
            Add
          </Button>
          <br />

          <br/>
          <br/>

          <Table id="movies-table" style={{marginTop:5}}>
            <thead>
              <tr>
                <th>Movie Name</th>
                <th>Time</th>
                <th>Projection Type</th>
                <th>Hall</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.projections.map((projection) => {
                return (
                  <tr key={projection.id}>
                    <td>{projection.filmNaziv}</td>
                    <td>{projection.datumIVreme}</td>
                    <td>{projection.tip}</td>
                    <td>{projection.sala}</td>
                    <td>{projection.cenaKarte}</td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
          <ButtonGroup>
            <Button disabled={this.state.pageNo===0} onClick={()=> this.getProjections(this.state.pageNo-1)}>Prev</Button>
            <Button disabled={this.state.pageNo===this.state.totalPages-1} onClick={()=> this.getProjections(this.state.pageNo+1)}>Next</Button>
          </ButtonGroup>
        </div>
      </div>
    );
  }
}

export default Projections;
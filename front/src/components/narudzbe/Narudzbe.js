import React from "react";
import {Button, ButtonGroup, Table, Form, Col, Row, Collapse} from 'react-bootstrap';
import Test3Axios from "../../apis/Test3Axios";
import { BsFillTrashFill } from "react-icons/bs";
import { BsArrowRightShort } from "react-icons/bs";
import { BsArrow90DegLeft } from "react-icons/bs";
import { BsArrow90DegRight } from "react-icons/bs";

class Narudzbe extends React.Component {

    constructor(props){
        super(props);

        const search = {
            dostavljacId: -1,
            mestoIsporuke: "",
            pretragaDatumString: ""
        }

        this.state = {
            narudzbe: [],
            pageNo: 0,
            totalPages: 1,
            search: search,
            dostavljaci: [],
            showSearch: false,
            zbirCenaNarudzbiPoDostavljacu: ""
        }
    }

    componentDidMount(){
        this.getDostavljaci();
        this.getNarudzbe(0, null, null);
    }

    async getDostavljaci(){
        try{
            const res = await Test3Axios.get("/dostavljaci");
            this.setState({
                dostavljaci: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    async getNarudzbe(newPageNo, searchParamName, searchParamValue){
        const config = {
            params: {
                pageNo: newPageNo
            }
        }
        if (this.state.search.dostavljacId !== -1){
            config.params["dostavljacId"] = this.state.search.dostavljacId;
        }
        if (this.state.search.mestoIsporuke !== ""){
            config.params["mestoIsporuke"] = this.state.search.mestoIsporuke;
        }
        if (this.state.search.pretragaDatumString !== ""){
            config.params["pretragaDatumString"] = this.state.search.pretragaDatumString;
        }

        if (searchParamName === "dostavljacId") {
            config.params["dostavljacId"] = searchParamValue;
        }
        if (searchParamName === "mestoIsporuke") {
            config.params["mestoIsporuke"] = searchParamValue;
        }
        if (searchParamName === "pretragaDatumString") {
            config.params["pretragaDatumString"] = searchParamValue;
        }

        console.log(config);

        try{
            const res = await Test3Axios.get("/narudzbe", config);
            console.log(res.data);
            this.setState({
                narudzbe: res.data,
                pageNo: newPageNo,
                totalPages: res.headers["total-pages"]
            })
            if (res.headers["zbir-cena-dostavljac"]){
                this.setState({
                    zbirCenaNarudzbiPoDostavljacu: res.headers["zbir-cena-dostavljac"]
                })
            } else {
                this.setState({
                    zbirCenaNarudzbiPoDostavljacu: ""
                })
            }
        } catch(error){
            console.log(error)
        }
    }

    goToAdd(){
        this.props.history.push("/narudzbe/add")
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    getTodayDateString(){
        var todayFullDate = new Date();
        var todayFullDateString = todayFullDate.toISOString();
        var todayDateString = todayFullDateString.substring(0, 10);
        return todayDateString;
    }

    searchValueInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        let search = {...this.state.search};
        search[name] = value;
        this.setState({search: search});
        this.getNarudzbe(0, name, value);
    }

    searchForm(){
        return(
            <Collapse in={this.state.showSearch}>
                <Form style={{marginTop:35}} id="example-collapse-text">
                    <Col>
                        <Form.Group>
                            <Form.Label>Dostavljac</Form.Label>
                            <Form.Control
                             onChange={(e) => this.searchValueInputChange(e)}
                             name="dostavljacId"
                             value={this.state.search.dostavljacId}
                             as="select">
                                 <option value={-1}></option>
                                 {this.state.dostavljaci.map((dostavljac) =>{
                                     return(
                                         <option value={dostavljac.id} key={dostavljac.id}>
                                             {dostavljac.imePrezime}
                                         </option>
                                     )
                                 })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Mesto isporuke</Form.Label>
                            <Form.Control
                            value={this.state.search.mestoIsporuke}
                            name="mestoIsporuke"
                            as="input"
                            type="text"
                            onChange={(e) => this.searchValueInputChange(e)}>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Narudzbe napravljene nakon datuma</Form.Label>
                            <Form.Control
                            value={this.state.search.pretragaDatumString}
                            name="pretragaDatumString"
                            as="input"
                            type="date"
                            onChange={(e) => this.searchValueInputChange(e)}>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    {/* <Col>
                        <Button onClick={() => this.getLinije(0)}>Pretrazi</Button>
                    </Col> */}
                </Form>
            </Collapse>
        )
    }

    searchShowHideCheckbox(){
        return(
            <>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check onChange={() => {this.setState({showSearch: !this.state.showSearch})}} type="checkbox"
                    aria-controls="example-collapse-text" aria-expanded={this.state.showSearch} label={this.state.showSearch? "Hide Search":"Show Search"}>
                    </Form.Check>
                </Form.Group>
            </>
        )

    }

    // searchShowHideButton(){
    //     return(
    //         <Button variant="outline-dark" size="sm" onClick={() => {this.setState({showSearch: !this.state.showSearch})}}
    //         aria-controls="example-collapse-text" aria-expanded={this.state.showSearch}>
    //             {(this.state.showSearch)? "Hide" : "Show"} Search Form
    //         </Button>
    //     )
    // }

    async delete(narudzbaId){
        try{
            const res = await Test3Axios.delete("/narudzbe/" + narudzbaId);
            console.log(res.data)
            alert("Narudzba je izbrisana.");
            this.deleteNarudzbaFromState(narudzbaId);
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    deleteNarudzbaFromState(narudzbaId){
        var narudzbe = this.state.narudzbe;
        narudzbe.forEach((narudzba, index) =>{
            if (narudzba.id === narudzbaId){
                narudzbe.splice(index, 1);
                this.setState({narudzbe: narudzbe});
            }
        })
    }

    zbirCenaNarudzbiPoDostavljacu(){
        if (this.state.zbirCenaNarudzbiPoDostavljacu != ""){
            return(
                <tr>
                    <td colSpan="3">Ukupni zbir cena narudzbi za odabranog dostavljaca:</td>
                    <td>{this.state.zbirCenaNarudzbiPoDostavljacu}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            )
        }
    }

    getTodayDateTimeString(){
        var todayFullDateTime = new Date();
        var todayFullDateTimeString = todayFullDateTime.toISOString();
        var todayDateTimeString = todayFullDateTimeString.substring(0, 16);
        return todayDateTimeString;
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    async rezervisi(linijaId){
        var rezervacijaDTO = {
            linijaId: linijaId
        };
        console.log(rezervacijaDTO);

        try{
            const res = await Test3Axios.post("/rezervacije", rezervacijaDTO);
            console.log(res);
            alert("Uspesno ste rezervisali kartu.")
            window.location.reload()
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    proveraDostupnaMesta(linija){
        if (linija.brojMesta > 0){
            return false;
        } else {
            return true;
        }
    }

    async kreirajRacun(narudzbaId){
        var racunDTO = {
            narudzbaId: narudzbaId
        };
        console.log(racunDTO);

        try{
            const res = await Test3Axios.post("/racuni", racunDTO);
            console.log(res);
            alert("Racun je uspesno kreiran.")
            window.location.reload();
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    vidiRacun(racunId){
        this.props.history.push("/racuni/" + racunId);
    }

    kreirajVidiRacun(narudzba){
        if (narudzba.racunId == undefined){
            return(
                <Button variant="warning" onClick={() => this.kreirajRacun(narudzba.id)}>Kreiraj racun</Button>
            )
        } else {
            return (
                <Button onClick={() => this.vidiRacun(narudzba.racunId)}>Vidi racun</Button>
            )
        }
    }

    render(){
        return(
            <div className="container">
                <div className="jumbotron bg-secondary text-white">
                    <h2 className="display-4">Prikaz narudzbi</h2>
                    <hr className="my-4"></hr>
                    {window.localStorage['role'] === "ROLE_ADMIN"?
                    <Button variant="outline-light" size="sm" onClick={() => this.goToAdd()}>Dodaj narudzbu</Button>:null}
                    {/* {this.kreirajLinijuForma()} */}
                </div>
                {this.searchShowHideCheckbox()}
                {this.searchForm()}
                <ButtonGroup className="float-right p-2">
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===0} 
                    onClick={() => this.getNarudzbe(this.state.pageNo-1, null, null)}><BsArrow90DegLeft/></Button>
                    <Button variant="outline-dark" size="sm" disabled={this.state.pageNo===this.state.totalPages-1} 
                    onClick={() => this.getNarudzbe(this.state.pageNo+1, null, null)}><BsArrow90DegRight/></Button>
                </ButtonGroup>
                <Table striped hover variant="dark" style={{marginTop:5}}>
                    <thead style={{backgroundColor: 'black', color: 'white'}}>
                        <tr>
                            <th>Broj narudzbe</th>
                            <th>Datum</th>
                            <th>Mesto isporuke</th>
                            <th>Cena</th>
                            <th>Opis</th>
                            <th>Dostavljac</th>
                            <th>Kreiraj/vidi racun</th>
                            {window.localStorage['role']==="ROLE_ADMIN"?
                            <th colSpan="1">Brisanje</th>:null}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.narudzbe.map((narudzba) =>{
                            return(
                                <tr key={narudzba.id}>
                                    <td>{narudzba.brNarudzbe}</td>
                                    <td>{this.formatDate(narudzba.datum)}</td>
                                    <td>{narudzba.mestoIsporuke}</td>
                                    <td>{narudzba.cena}</td>
                                    <td>{narudzba.opis}</td>
                                    <td>{narudzba.dostavljacImePrezime}</td>
                                    <td>{this.kreirajVidiRacun(narudzba)}</td>
                                    {/* {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="warning" onClick={() => this.goToEdit(narudzba.id)}>Izmeni</Button></td>:null} */}
                                    {window.localStorage['role']==="ROLE_ADMIN"?
                                    <td><Button variant="danger" onClick={() => this.delete(narudzba.id)}><BsFillTrashFill/></Button></td>:null}
                                    {/* {window.localStorage['role']==="ROLE_ADMIN"?
                                    [
                                    <td><Button disabled={zadatak.stanjeId==3} onClick={()=> this.promeniStanje(zadatak.id)}>Predji na sledece stanje</Button></td>,
                                    <td><Button onClick={()=> this.goToEdit(zadatak.id)}>Edit</Button></td>
                                    ]:null} */}
                                </tr>
                            )
                        })}
                        {this.zbirCenaNarudzbiPoDostavljacu()}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Narudzbe;
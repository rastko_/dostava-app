import React from "react";
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import Test3Axios from "../../apis/Test3Axios";

class VidiRacun extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            racunId: this.props.match.params.racunId,
            brRacuna: "",
            datum: "",
            ukupnaCena: ""
        }
    }

    componentDidMount(){
        this.getRacun(this.props.match.params.racunId);
    }

    async getRacun(racunId){

        console.log(racunId);
        try{
            const res = await Test3Axios.get("/racuni/" + racunId);
            console.log(res.data);
            this.setState({
                brRacuna: res.data.brRacuna,
                datum: res.data.datum,
                ukupnaCena: res.data.ukupnaCena
            })
        } catch(error){
            console.log(error)
        }
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    render(){
        return(
            <div>
                <h4>Prikaz racuna</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>Broj racuna</Form.Label>
                            <Form.Control
                            value={this.state.brRacuna}
                            name="brRacuna"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            disabled
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Datum</Form.Label>
                            <Form.Control
                            value={this.formatDate(this.state.datum)}
                            name="datum"
                            as="input"
                            type="text"
                            disabled
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Ukupna cena</Form.Label>
                            <Form.Control
                            value={this.state.ukupnaCena}
                            name="ukupnaCena"
                            as="input"
                            type="number"
                            step="0.01"
                            min="0"
                            disabled
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                </Form>
            </div>
            
        )
    }
}

export default VidiRacun;

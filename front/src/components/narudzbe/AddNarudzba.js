import React from "react";
import { Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import Test3Axios from "../../apis/Test3Axios";

class AddNarudzba extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            brNarudzbe: "",
            datum: "",
            mestoIsporuke: "",
            cena: "",
            opis: "",
            dostavljacId: -1,
            dostavljaci: [],
        }
    }

    componentDidMount(){
        this.getDostavljaci();
    }

    async getDostavljaci(){
        try{
            const res = await Test3Axios.get("/dostavljaci");
            this.setState({
                dostavljaci: res.data
            })
        } catch(error){
            console.log(error)
        }
    }

    formatDate(date){
        return date.replace("T", " ");
    }

    async create(){
        var narudzbaDTO = {
            brNarudzbe: this.state.brNarudzbe,
            datum: this.formatDate(this.state.datum),
            mestoIsporuke: this.state.mestoIsporuke,
            cena: this.state.cena,
            opis: this.state.opis,
            dostavljacId: this.state.dostavljacId
        };
        console.log(narudzbaDTO);

        if(narudzbaDTO.brNarudzbe === "" || narudzbaDTO.datum === "" || narudzbaDTO.mestoIsporuke === "" 
        || narudzbaDTO.cena === "" || narudzbaDTO.opis === "" || narudzbaDTO.dostavljacId === -1){
            alert("Niste ispravno uneli podatke.");
            return;
        }

        try{
            const res = await Test3Axios.post("/narudzbe", narudzbaDTO);
            console.log(res);
            alert("Narudzba je uspesno dodata.")
            this.props.history.push("/narudzbe");
        } catch(error){
            console.log(error);
            alert("Desila se greska!")
        }
    }

    onInputChange(e){
        const name = e.target.name;
        const value = e.target.value;

        var change = {};
        change[name] = value;
        this.setState(change);
    }

    getTodayDateTimeString(){
        var todayFullDateTime = new Date();
        var todayFullDateTimeString = todayFullDateTime.toISOString();
        var todayDateTimeString = todayFullDateTimeString.substring(0, 16);
        return todayDateTimeString;
    }

    getTodayDateString(){
        var todayFullDate = new Date();
        var todayFullDateString = todayFullDate.toISOString();
        var todayDateString = todayFullDateString.substring(0, 10);
        return todayDateString;
    }

    render(){
        return(
            <div>
                <h4>Dodaj novu narudzbu</h4>
                <Form style={{marginTop:35}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>Broj narudzbe</Form.Label>
                            <Form.Control
                            value={this.state.brNarudzbe}
                            name="brNarudzbe"
                            as="input"
                            type="number"
                            step="1"
                            min="0"
                            placeholder="Broj narudzbe"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Datum</Form.Label>
                            <Form.Control
                            value={this.state.datum}
                            name="datum"
                            as="input"
                            type="date"
                            min = {this.getTodayDateString()}
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Mesto isporuke</Form.Label>
                            <Form.Control
                            value={this.state.mestoIsporuke}
                            name="mestoIsporuke"
                            as="input"
                            type="text"
                            placeholder="Mesto isporuke"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Cena narudzbe</Form.Label>
                            <Form.Control
                            value={this.state.cena}
                            name="cena"
                            as="input"
                            type="number"
                            step="0.01"
                            min="0"
                            placeholder="Cena narudzbe"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Opis</Form.Label>
                            <Form.Control
                            value={this.state.opis}
                            name="opis"
                            as="input"
                            type="text"
                            placeholder="Opis"
                            onChange={(e) => this.onInputChange(e)}
                            ></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Dostavljac</Form.Label>
                            <Form.Control
                            value={this.state.dostavljacId}
                            name="dostavljacId"
                            as="select"
                            onChange={(e) => this.onInputChange(e)}>
                                <option value={-1}></option>
                                {this.state.dostavljaci.map((dostavljac) =>{
                                    return(
                                        <option value={dostavljac.id} key={dostavljac.id}>
                                            {dostavljac.imePrezime}
                                        </option>
                                    )
                                })}
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <br/>
                    <Col>
                        <Button variant="outline-dark" size="sm" onClick={() => this.create()}>Dodaj narudzbu</Button>
                    </Col>
                </Form>
            </div>
            
        )
    }
}

export default AddNarudzba;
